FROM alpine:latest

RUN apk add --no-cache openssh-client bash

ADD run.sh /

ENTRYPOINT ["sh", "-c", "/run.sh"]
