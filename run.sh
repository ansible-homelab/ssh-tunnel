#!/bin/sh

if  [ -z ${SSH_PORT+x} ] ; then
    SSH_PORT="22"
fi

variable=$PORTS
for i in ${variable//","/ }
do
  ports=${i//":"/ }
  remote_port=${ports%% *}
  container_port=${ports##"$remote_port" }
  links="${links}-L $container_port:$REMOTE_IP:$remote_port "
done

mkdir -p ~/.ssh # Make .ssh dir if it does not exist
touch ~/.ssh/known_hosts # In case known_hosts does not exist
if ! grep "$(ssh-keyscan $SSH_HOST 2>/dev/null)" ~/.ssh/known_hosts > /dev/null; then
    ssh-keyscan $SSH_HOST >> ~/.ssh/known_hosts
fi

if  [ -n "$SSH_KEY" ] ; then
    extra_options="-i $SSH_KEY"
fi

echo "Starting SSH tunnel to $SSH_HOST and forwaring ports to $REMOTE_IP"

# -N | Do not execute a remote command.
# -T | Disable pseudo-terminal allocation.
# -o GatewayPorts=true | Specifies whether remote hosts are allowed to connect to local forwarded ports.
# -o ExitOnForwardFailure=yes | Terminate the connection if the requested dynamic, tunnel, local, and remote port forwardings fail.
ssh -N -T \
-o GatewayPorts=true \
-o ExitOnForwardFailure=yes \
$links \
$SSH_USER@$SSH_HOST \
-p $SSH_PORT \
$extra_options
# tail -f /dev/null
